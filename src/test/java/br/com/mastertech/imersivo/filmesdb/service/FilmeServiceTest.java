package br.com.mastertech.imersivo.filmesdb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;

//@SpringBootTest - executa o projeto completo com o springboot e acesso a bases de dados
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {FilmeService.class})
public class FilmeServiceTest {
	@Autowired
	FilmeService filmeService;
	
	@MockBean
	FilmeRepository filmeRepository;
	
	@Test
	public void deveListarTodosOsFilmes() {

		//setup
		Filme filme = new Filme();
		
		List<Filme> filmes = new ArrayList<>();
		
		filmes.add(filme);
		
		Mockito.when(filmeRepository.findAll()).thenReturn(filmes);
		
		//action
		//Iterable<Filme> resultado = filmeService.obterFilmes();
		List<Filme> resultado = Lists.newArrayList(filmeService.obterFilmes());
		
		//check
		//Iterator<Filme> iterador = resultado.iterator();
		//Filme filmeResultado = iterador.next();
		//assertNotNull(filmeResultado);
		assertNotNull(resultado.get(0));
		assertEquals(filme, resultado.get(0));
		
	}
	
	@Test
	public void deveListarTodosOsFilmesDeUmDiretor() {
		
		//Setup
		long id = 1;
		
		FilmeDTO filmeDTO = Mockito.mock(FilmeDTO.class);
		List<FilmeDTO> filmes = new ArrayList<>();
		filmes.add(filmeDTO);
		
		Mockito.when(filmeRepository.findAllByDiretor_Id(id)).thenReturn(filmes);
		
		//Action
		List<FilmeDTO> resultado = Lists.newArrayList(filmeService.obterFilmes(id));
		
		//check
		assertEquals(filmeDTO, resultado.get(0));
		
	}

	@Test
	public void deveCriarUmFilme() {
		//setup
		Filme filme = new Filme();
		
		//action
		filmeService.criarFilme(filme);
		
		//check
		Mockito.verify(filmeRepository).save(filme);
		
	}
	
	@Test
	public void deveEditarUmFilme() {
		//setup
		Long id = 1L;
		Long novoId = 2L;
		String novoGenero = "Comédia";
		String novoTitulo = "Bob";
		
		Filme filmeDoBanco = criarFilme(id);
		Filme filmeDoFront = criarFilme(id);
		
		filmeDoFront.setId(novoId);
		filmeDoFront.setTitulo(novoTitulo);
		filmeDoFront.setGenero(novoGenero);
		
		Mockito.when(filmeRepository.findById(id)).thenReturn(Optional.of(filmeDoBanco));
		Mockito.when(filmeRepository.save(filmeDoBanco)).thenReturn(filmeDoBanco);
		
		
		//action
		Filme resultado = filmeService.editarFilme(id, filmeDoFront);
		
		//check
		Mockito.verify(filmeRepository).save(filmeDoBanco);
		assertNotNull(resultado);
		assertEquals(novoGenero, resultado.getGenero());
		assertNotEquals(novoTitulo, resultado.getTitulo());
		assertNotEquals(novoId, resultado.getId());
	}

	@Test
	public void deveApagarUmFilme() {
		//setup	
		Long id = 1L;
		
		Filme filme = new Filme();

		Mockito.when(filmeRepository.findById(id)).thenReturn(Optional.of(filme));		

		//action
		filmeService.apagarFilme(id);

		//check
		Mockito.verify(filmeRepository).delete(filme);
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOFilme() {
		//setup
		Long id = 1L;
		Long novoId = 2L;
		
		Filme filmeDoFront = criarFilme(novoId);
				
		Mockito.when(filmeRepository.findById(id)).thenReturn(Optional.empty());
		
		//action
		Filme resultado = filmeService.editarFilme(id, filmeDoFront);
		
		//check
		//Mockito.verify(filmeRepository).save(filmeDoBanco);

	}

	private Filme criarFilme(long id) {
		Filme filme = new Filme();
		filme.setId(id);
		filme.setTitulo("Matrix");
		filme.setGenero("Fantasia");
//		filme.setDiretor(1L);
//		filme.setData("20190829T12:53:00");		
		return filme;
	}
}

	