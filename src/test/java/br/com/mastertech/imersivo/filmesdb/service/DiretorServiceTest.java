package br.com.mastertech.imersivo.filmesdb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Diretor;
import br.com.mastertech.imersivo.filmesdb.model.DiretorDTO;
import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.DiretorRepository;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DiretorService.class})
public class DiretorServiceTest {
	@Autowired
	DiretorService diretorService;
	
	@MockBean
	DiretorRepository diretorRepository;
	
	@MockBean
	FilmeRepository filmeRepository;
	
	@MockBean
	FilmeService filmeService;
		
	@Test
	public void deveListarTodosOsDiretores() {

		//setup
		Diretor diretor = new Diretor();
		
		List<Diretor> diretores = new ArrayList<>();
		
		diretores.add(diretor);
		
		Mockito.when(diretorRepository.findAll()).thenReturn(diretores);
		
		//action
		List<Diretor> resultado = Lists.newArrayList(diretorService.obterDiretores());
		
		//check
		assertNotNull(resultado.get(0));
		assertEquals(diretor, resultado.get(0));
		
	}
	
	@Test
	public void deveListarTodosOsFilmesDeUmDiretor() {
		
		//Setup
		long id = 1;
		
		FilmeDTO filmeDTO = Mockito.mock(FilmeDTO.class);
		List<FilmeDTO> filmes = new ArrayList<>();
		filmes.add(filmeDTO);
		
//		DiretorDTO diretorDTO = Mockito.mock(DiretorDTO.class);
//		List<DiretorDTO> diretores = new ArrayList<>();
//		diretores.add(diretorDTO);
//		
		
		Mockito.when(filmeService.obterFilmes(id)).thenReturn(filmes);
		
		//Action
		List<FilmeDTO> resultado = Lists.newArrayList(diretorService.obterFilmes(id));
		//List<DiretorDTO> resultDir = Lists.newArrayList(diretorService.obterDiretor(id));
		

		//Mockito.verify(diretorRepository).findSummary(id);

		
		//check
		assertEquals(filmeDTO, resultado.get(0));
		//assertEquals(diretorDTO, resultDir.get(0));
		
		assertNotNull(resultado.get(0));		
		//assertNotNull(resultDir.get(0));

		
	}

//	@Test
//	public void testeDeveListarTodosOsFilmesDeUmDiretor() {
//		
//		//Setup
//		Long id = 1L;
//		Diretor diretor = new Diretor();
//		diretor.setId(id);
//		diretor.setIdade(45);
//		diretor.setNome("Ana");
//		
//		Filme filme = criarFilme(id);
//		
//		FilmeDTO filmeDTO = Mockito.mock(FilmeDTO.class);
//		List<FilmeDTO> filmes = new ArrayList<>();
//		filmes.add(filmeDTO);
//		
//		Mockito.when(filmeService.obterFilmes(id)).thenReturn(filmes);
//		
//		//Action
//		List<FilmeDTO> resultado = Lists.newArrayList(filmeService.obterFilmes(id));
//		
//		
//		diretorRepository.findSummary(id);
//		
//		//check
////		assertEquals(filmeDTO, resultado.get(0));
//		assertNotNull(resultado.get(0));
//		
//		
//	}

	@Test
	public void deveCriarUmDiretor() {
		//setup
		Diretor diretor = new Diretor();
		
		//action
		diretorService.criarDiretor(diretor);
		
		//check
		Mockito.verify(diretorRepository).save(diretor);
		
	}


	@Test
	public void deveApagarUmDiretor() {
		//setup	
		Long id = 1L;
		
		Diretor diretor = new Diretor();

		Mockito.when(diretorRepository.findById(id)).thenReturn(Optional.of(diretor));		

		//action
		diretorService.apagarDiretor(id);

		//check
		Mockito.verify(diretorRepository).delete(diretor);
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOFilme() {
		//setup
		Long id = 1L;
				
		Mockito.when(diretorRepository.findById(id)).thenReturn(Optional.empty());
		
		//action
		Diretor resultado = diretorService.encontraOuDaErro(id);
		
		//check
		//Mockito.verify(filmeRepository).save(filmeDoBanco);

	}

}
	
