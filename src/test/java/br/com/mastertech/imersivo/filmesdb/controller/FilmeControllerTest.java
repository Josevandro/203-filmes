package br.com.mastertech.imersivo.filmesdb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.service.DiretorService;
import br.com.mastertech.imersivo.filmesdb.service.FilmeService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {FilmeController.class})
public class FilmeControllerTest{
	
	@MockBean
	FilmeService filmeService;
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void deveObterListadeFilmes() throws Exception{
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setGenero("Drama");
		
		List<Filme> filmes = Lists.newArrayList(filme);

		Mockito.when(filmeService.obterFilmes()).thenReturn(filmes);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/filme"))
		.andExpect(status().isOk())
		.andExpect(content().string(containsString("1")))
		.andExpect(content().string(containsString("Drama")));
	}
	
	@Test
	public void deveApagarUmFilme() throws Exception{
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setGenero("Drama");
		
		mockMvc.perform(MockMvcRequestBuilders.delete("/filme/1"))
		.andExpect(status().isNoContent());

		
		Mockito.verify(filmeService).apagarFilme(filme.getId());
		
	}
	
	@Test
	public void deveCriarUmFilme() throws JsonProcessingException, Exception{
		
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setTitulo("Crepusculo");
		filme.setGenero("Drama");
		
		//List<Filme> filmes = Lists.newArrayList(filme);

		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(MockMvcRequestBuilders.post("/filme")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(filme)))
				.andExpect(status().isCreated());
		
		Mockito.verify(filmeService).criarFilme(Mockito.any(Filme.class));
		
	}

	@Test
	public void deveEditarUmFilme() throws JsonProcessingException, Exception{
		
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setTitulo("Crepusculo");
		filme.setGenero("Drama");

		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(MockMvcRequestBuilders.patch("/filme/1")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(filme)))
				.andExpect(status().isOk());
		
		Mockito.when(filmeService.editarFilme(1L, filme)).thenReturn(filme);
		
	}
}
